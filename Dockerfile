FROM node:14-alpine

WORKDIR /backend

RUN npm install -g ts-node

COPY ["package.json", "package-lock.json", "./"]

RUN npm install

COPY . .

CMD npm start
